# Al Punto

Backend to Al Punto software, GPS smart tracker

## Contributing

### Issue Tags
Issues will contain one of the following tags:

- Requirements: `#d4c5e2`
- Design: `#c2e812`
- Code: `#bc4b51`
- Test: `#2e933c`
- Deployment: `#3f88c5`
- Maintenance or Metrics: `#45425a`
- Bug: `#e76f51`

### Commits

The project will follow the [Angular guide](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#type) for commit types.

>* **build**: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
>* **ci**: Changes to our CI configuration files and scripts (example scopes: Circle, BrowserStack, SauceLabs)
>* **docs**: Documentation only changes
>* **feat**: A new feature
>* **fix**: A bug fix
>* **perf**: A code change that improves performance
>* **refactor**: A code change that neither fixes a bug nor adds a feature
>* **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
>* **test**: Adding missing tests or correcting existing tests

As well as the following commit format

```git
<type>: <subject-less-than-140-chars>
<BLANK LINE>
<body>
<BLANK LINE>
<breaking changes, links or references>
```